﻿namespace DownloadImageConsole;

public class DownloaderEventArgs : EventArgs
{
    public string RemoteUri { get; set; }
    public string LocalPath { get; set; }

    public DownloaderEventArgs(string remoteUri, string localPath)
    {
        RemoteUri = remoteUri;
        LocalPath = localPath;
    }
}
