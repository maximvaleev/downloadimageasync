﻿/* Домашнее задание

Делегаты, Event-ы, добавляем асинхронное выполнение

    Напишите класс ImageDownloader. В этом классе должен быть метод Download, который скачивает картинку из интернета. 
        Для загрузки картинки можно использовать примерно такой код: https://dotnetfiddle.net/5oT1Hi

    // Откуда будем качать
    string remoteUri = "https://effigis.com/wp-content/uploads/2015/02/Iunctus_SPOT5_5m_8bit_RGB_DRA_torngat_mountains_national_park_8bits_1.jpg";
    // Как назовем файл на диске
    string fileName = "bigimage.jpg";
    // Качаем картинку в текущую директорию
    var myWebClient = new WebClient();
    Console.WriteLine("Качаю \"{0}\" из \"{1}\" .......\n\n", fileName, remoteUri);
    myWebClient.DownloadFile(remoteUri, fileName);
    Console.WriteLine("Успешно скачал \"{0}\" из \"{1}\"", fileName, remoteUri);

    Создайте экземпляр этого класса и вызовите скачивание большой картинки, например,
        https://effigis.com/wp-content/uploads/2015/02/Iunctus_SPOT5_5m_8bit_RGB_DRA_torngat_mountains_national_park_8bits_1.jpg
    В конце работы программы выведите в консоль "Нажмите любую клавишу для выхода" и ожидайте нажатия любой клавиши.
    Добавьте события: в классе ImageDownloader в начале скачивания картинки и в конце скачивания картинки выкидывайте 
        события (event) ImageStarted и ImageCompleted соответственно.
    В основном коде программы подпишитесь на эти события, а в обработчиках их срабатываний выводите соответствующие 
        уведомления в консоль: "Скачивание файла началось" и "Скачивание файла закончилось".
    Сделайте метод ImageDownloader.Download асинхронным. Если Вы скачивали картинку с использованием WebClient.DownloadFile, 
        то используйте теперь WebClient.DownloadFileTaskAsync - он возвращает Task.
    В конце работы программы выводите теперь текст "Нажмите клавишу A для выхода или любую другую клавишу для проверки статуса скачивания" и 
        ожидайте нажатия любой клавиши. Если нажата клавиша "A" - выходите из программы. В противном случае выводите 
        состояние загрузки картинки (True - загружена, False - нет). Проверить состояние можно через вызов Task.IsCompleted.
    Поздравляю! Ваша загрузка картинки работает асинхронно с основным потоком консоли.
    
    5) Модифицируйте программу таким образом, чтобы она скачивала сразу 10 картинок одновременно, 
        останавливая одновременно все потоки по нажатию кнопки "A". По нажатию других кнопок выводить на экран список 
        загружаемых картинок и их состояния - загружены или нет, аналогично выводу для одной картинки.

https://stsci-opo.org/STScI-01H0N3J0FZS2HPRTX17ZPJ91DW.png - картинка 35 Мб
https://stsci-opo.org/STScI-01GYAFZCZK67B2X5EFJ721GFM5.png - картинка 180 Мб
 */

namespace DownloadImageConsole;

internal class Program
{
    static void Main()
    {
        string imageUri = @"https://stsci-opo.org/STScI-01H0N3J0FZS2HPRTX17ZPJ91DW.png";

        ImageDownloader downloader = new();
        downloader.RaiseStartDownloadingEvent += HandleStartDownloadingEvent;
        downloader.RaiseFinishDownloadingEvent += HandleFinishDownloadingEvent;

        Dictionary<string, Task> tasks = CreateDownloadTasks(imageUri, downloader);
        UserInteractions(tasks);

        downloader.RaiseStartDownloadingEvent -= HandleStartDownloadingEvent;
        downloader.RaiseFinishDownloadingEvent -= HandleFinishDownloadingEvent;
    }

    private static void HandleStartDownloadingEvent(object? sender, DownloaderEventArgs e)
    {
        Console.WriteLine($"Начато скачивание ссылки \"{e.RemoteUri}\" в файл \"{e.LocalPath}\"");
    }

    private static void HandleFinishDownloadingEvent(object? sender, DownloaderEventArgs e)
    {
        Console.WriteLine($"Завершено скачивание ссылки \"{e.RemoteUri}\" в файл \"{e.LocalPath}\"");
    }

    private static Dictionary<string, Task> CreateDownloadTasks(string imageUri, ImageDownloader downloader)
    {
        Dictionary<string, Task> tasks = new(10);
        for (int i = 0; i < 10; i++)
        {
            string path = "image" + i + ".png";
            tasks.Add(path, downloader.Download(imageUri, path));
        }
        return tasks;
    }

    private static void UserInteractions(Dictionary<string, Task> tasks)
    {
        Console.WriteLine("Начато скачивание изображения.");
        Console.WriteLine("Для отмены нажмите \"А\". Для проверки статуса скачивания нажмите любую клавишу");
        while (true)
        {
            char key = Console.ReadKey(intercept: true).KeyChar;
            if (!(key == 'a' || key == 'A'))
            {
                Console.WriteLine("Статусы загрузки:");
                foreach (KeyValuePair<string, Task> item in tasks)
                {
                    Console.WriteLine($"{item.Key} - {item.Value.IsCompleted}");
                }
            }
            else
            {
                Console.WriteLine("Выходим");
                break;
            }
        }
    }
}