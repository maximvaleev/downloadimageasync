﻿namespace DownloadImageConsole;

internal class ImageDownloader
{
    public event EventHandler<DownloaderEventArgs>? RaiseStartDownloadingEvent;
    public event EventHandler<DownloaderEventArgs>? RaiseFinishDownloadingEvent;

    public async Task Download(string remoteUri, string localPath)
    {
        // VS говорит что WebClient является Depricated. Нагуглил решение через HttpClient
        
        DownloaderEventArgs eventArgs = new(remoteUri, localPath);
        RaiseStartDownloadingEvent?.Invoke(this, eventArgs);

        using HttpClient httpClient = new();
        using Stream s = await httpClient.GetStreamAsync(remoteUri);
        using var fs = new FileStream(localPath, FileMode.OpenOrCreate);
        await s.CopyToAsync(fs);

        RaiseFinishDownloadingEvent?.Invoke(this, eventArgs);
    }
}
